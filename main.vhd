----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:02:17 09/02/2017 
-- Design Name: 
-- Module Name:    main - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity main is
	port (VERDE, AMARELO, VERMELHO, VERDE_PESSOA, VERMELHO_PESSOA : out std_logic;
			clk : in std_logic);
end main;

architecture Behavioral of main is
	signal counter : std_logic_vector(28 downto 0) := (others => '0');
	signal estado : std_logic_vector(2 downto 0) := (others => '0');
	signal estado_blink : std_logic;
begin
	estado <= counter(28 downto 26);
	estado_blink <= counter(23);

	clock : process(clk)
	begin
		if rising_edge(clk) then
			counter <= counter + 1;
		end if;
	end process;
	
	process(estado, estado_blink)
	begin
		case estado is
			when b"000" =>
				VERDE <= '1';
				AMARELO <= '0';
				VERMELHO <= '0';
				VERDE_PESSOA <= '0';
				VERMELHO_PESSOA <= '1';
			when b"001" =>
				VERDE <= '1';
				AMARELO <= '0';
				VERMELHO <= '0';
				VERDE_PESSOA <= '0';
				VERMELHO_PESSOA <= '1';
			when b"010" =>
				VERDE <= '0';
				AMARELO <= '1';
				VERMELHO <= '0';
				VERDE_PESSOA <= '0';
				VERMELHO_PESSOA <= '1';
			when b"011" =>
				VERDE <= '0';
				AMARELO <= '0';
				VERMELHO <= '1';
				VERDE_PESSOA <= '1';
				VERMELHO_PESSOA <= '0';
			when b"100" =>
				VERDE <= '0';
				AMARELO <= '0';
				VERMELHO <= '1';
				VERDE_PESSOA <= '1';
				VERMELHO_PESSOA <= '0';
			when b"101" =>
				VERDE <= '0';
				AMARELO <= '0';
				VERMELHO <= '1';
				VERDE_PESSOA <= '1';
				VERMELHO_PESSOA <= '0';
			when b"110" =>
				VERDE <= '0';
				AMARELO <= '0';
				VERMELHO <= '1';
				VERDE_PESSOA <= '0';
				if estado_blink = '1' then
					VERMELHO_PESSOA <= '1';
				else
					VERMELHO_PESSOA <= '0';
				end if;
			when b"111" =>
				VERDE <= '0';
				AMARELO <= '0';
				VERMELHO <= '1';
				VERDE_PESSOA <= '0';
				if estado_blink = '1' then
					VERMELHO_PESSOA <= '1';
				else
					VERMELHO_PESSOA <= '0';
				end if;
			when others =>
				VERDE <= '0';
				AMARELO <= '0';
				VERMELHO <= '0';
				VERDE_PESSOA <= '0';
				VERMELHO_PESSOA <= '0';
		end case;
	end process;
end Behavioral;
